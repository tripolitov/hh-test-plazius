﻿using System;
using NUnit.Framework;

namespace Plazius.RoutePlanner.Tests
{
    [TestFixture]
    class WaypointTests
    {
        [Test]
        public void FactoryMethodCreatesWaypointWithProvidedName()
        {
            var name = Guid.NewGuid().ToString();

            var waypoint = Waypoint.Create(name);

            Assert.AreEqual(name, waypoint.Name);
        }

        [Test]
        public void DefaultPropertyReturnsWaypointWithNullName()
        {
            var waypoint = Waypoint.Default;

            Assert.IsNull(waypoint.Name);
        }

        [Test]
        public void DefaultWaypointHasNullName()
        {
            var waypoint = default(Waypoint);

            Assert.IsNull(waypoint.Name);
        }

        [Test]
        public void WaypointCtorThrowsArgumentNullExceptionForNullName()
        {
            Assert.Throws<ArgumentNullException>(() => new Waypoint(default(string)));
        }

        [Test]
        public void WaypointCtorThrowArgumentExceptionForEmptyName()
        {
            Assert.Throws<ArgumentException>(() => new Waypoint(string.Empty));
        }

        [Test]
        public void WaypointCtorThrowsArgumentExceptionForWhitespaceName()
        {
            Assert.Throws<ArgumentException>(() => new Waypoint("  "));
        }

        [Test]
        public void WaypointImplicitlyConvertedToString()
        {
            var name = Guid.NewGuid().ToString();

            var waypoint = new Waypoint(name);

            string str = waypoint;

            Assert.AreEqual(name, str);
        }

        [Test]
        public void WaypointImplicitlyConvertedFromString()
        {
            var name = Guid.NewGuid().ToString();

            Waypoint waypoint = name;

            Assert.AreEqual(name, waypoint.Name);
        }

        [Test]
        public void WaypointThrowsArgumentNullExceptionWhenImplicitlyConvertedFromNullString()
        {
            var name = default(string);

            Assert.Throws<ArgumentNullException>(() =>
            {
                Waypoint waypoint = name;
            });
        }

        [Test]
        public void WaypointThrowsArgumentExceptionWhenImplicitlyConvertedFromEmptyString()
        {
            var name = string.Empty;

            Assert.Throws<ArgumentException>(() =>
            {
                Waypoint waypoint = name;
            });
        }

        [Test]
        public void WaypointThrowsArgumentExceptionWhenImplicitlyConvertedFromWhitespaceString()
        {
            var name = "  ";

            Assert.Throws<ArgumentException>(() =>
            {
                Waypoint waypoint = name;
            });
        }

        [Test]
        public void WaypointToStringReturnsWaypointName()
        {
            var name = Guid.NewGuid().ToString();

            var waypoint = new Waypoint(name);

            Assert.AreEqual(waypoint.ToString(), waypoint.Name);
        }

        [Test]
        public void EqualityOperatorReturnsTrueForDifferentWaypointsWithSameNames()
        {
            var name = Guid.NewGuid().ToString();

            var left = new Waypoint(name);
            var right = new Waypoint(name);

            Assert.True(left == right);
        }

        [Test]
        public void EqualityOperatorReturnsFalseForDifferentWaypointsWithDifferentNames()
        {
            var leftName = Guid.NewGuid().ToString();
            var left = new Waypoint(leftName);

            var rightName = Guid.NewGuid().ToString();
            var right = new Waypoint(rightName);

            Assert.False(left == right);
        }

        [Test]
        public void NonEqualityOperatorReturnsFalseForDifferentWaypointsWithSameNames()
        {
            var name = Guid.NewGuid().ToString();

            var left = new Waypoint(name);
            var right = new Waypoint(name);

            Assert.False(left != right);
        }

        [Test]
        public void NonEqualityOperatorReturnsTrueForDifferentWaypointsWithDifferentNames()
        {
            var leftName = Guid.NewGuid().ToString();
            var left = new Waypoint(leftName);

            var rightName = Guid.NewGuid().ToString();
            var right = new Waypoint(rightName);

            Assert.True(left != right);
        }
    }
}
