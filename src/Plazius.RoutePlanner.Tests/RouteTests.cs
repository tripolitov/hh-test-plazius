﻿using System;
using System.Collections;
using System.Linq;
using NUnit.Framework;
using Plazius.RoutePlanner.Tests.Helpers;

namespace Plazius.RoutePlanner.Tests
{
    [TestFixture]
    class RouteTests
    {
        [Test]
        [TestCaseSource(nameof(OptimizeRouteTestData))]
        public Route OptimizeRoute(Route unsorted)
        {
            return unsorted.Optimize();
        }

        /// <summary>
        /// Test Case Source for <see cref="OptimizeRoute"/> unit test
        /// </summary>
        public static IEnumerable OptimizeRouteTestData
        {
            get
            {
                yield return Route.Empty.ToTestCaseData();

                yield return new Route(new[] { new Edge("A", "B") }).ToTestCaseData();

                yield return new Route(new[] { new Edge("A", "B"), new Edge("B", "C") }).ToTestCaseData();

                yield return new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "D") }).ToTestCaseData();

                yield return new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "D"), new Edge("D", "E") }).ToTestCaseData();

                yield return new Route(new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "D"), new Edge("D", "E"), new Edge("E", "F") })).ToTestCaseData();

                //NOTE[MT]: all explicit test case data above is actually replaced with generator below, 
                //          it is kept only for better comprehension of how test case data generator works.

                // create an array of letters from A to Z
                var letters = Enumerable.Range(65, 26).Select(i => ((char)i).ToString()).ToArray();
                
                // use letters to create waypoints with names from A to Z and from AA to ZZ
                var waypoints = letters.Concat(letters.SelectMany(x => letters, (a, b) => a + b)).Select(Waypoint.Create).ToArray();

                // create number of tests cases with increasing route length
                for (int i = 9; i < 702; i += 7)
                {
                    yield return new Route(waypoints.Take(i).Zip(waypoints.Skip(1).Take(i - 1), Edge.Create)).ToTestCaseData();
                }
            }
        }

        [Test]
        public void OptimizeThrowsExceptionForRouteWithGap()
        {
            var unsorted = new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "D"),  new Edge("E", "F") });
            Assert.Throws<GapDetectedException>(() => unsorted.Optimize());
        }

        [Test]
        public void OptimizeThrowsExceptionForRouteOfLength2WithCircle()
        {
            var unsorted = new Route(new[] { new Edge("A", "B"), new Edge("B", "A") });
            Assert.Throws<CircleDetectedException>(() => unsorted.Optimize());
        }

        [Test]
        public void OptimizeThrowsExceptionForRouteOfLength3WithSmallCircle()
        {
            var unsorted = new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "B") });
            Assert.Throws<CircleDetectedException>(() => unsorted.Optimize());
        }

        [Test]
        public void OptimizeThrowsExceptionForRouteOfLength3WithLargeCircle()
        {
            var unsorted = new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "A") });
            Assert.Throws<CircleDetectedException>(() => unsorted.Optimize());
        }

        [Test]
        public void OptimizeThrowsExceptionForRouteOfLength5WithSmallCircle()
        {
            var unsorted = new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "B"), new Edge("B", "E"), new Edge("E", "A") });
            Assert.Throws<CircleDetectedException>(() => unsorted.Optimize());
        }

        [Test]
        public void OptimizeThrowsExceptionForRouteOfLength5WithLargeCircle()
        {
            var unsorted = new Route(new[] { new Edge("A", "B"), new Edge("B", "C"), new Edge("C", "D"), new Edge("D", "E"), new Edge("E", "A") });
            Assert.Throws<CircleDetectedException>(() => unsorted.Optimize());
        }

        [Test]
        public void ToStringReturnsEmptyStringForEmptyRoute()
        {
            var str = Route.Empty.ToString();

            Assert.AreEqual(String.Empty, str);
        }

        [Test]
        public void ToStringReturnsSingleEdgeStringForOneEdgeRoute()
        {
            var edge = new Edge("A", "B");
            var str = new Route(new[] { edge}).ToString();

            Assert.AreEqual($"{edge.From}->{edge.To}", str);
        }

        [Test]
        public void ToStringReturnsEdgesStringFDelimitedBySemicolonForRoute()
        {
            var edge1 = new Edge("A", "B");
            var edge2 = new Edge("X", "Y");
            var edge3 = new Edge("Z", "T");

            var str = new Route(new[] { edge1, edge2, edge3 }).ToString();

            Assert.AreEqual($"{edge1.From}->{edge1.To}; {edge2.From}->{edge2.To}; {edge3.From}->{edge3.To}", str);
        }
    }
}
