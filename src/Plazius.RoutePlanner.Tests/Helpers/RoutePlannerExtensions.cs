﻿using NUnit.Framework;

namespace Plazius.RoutePlanner.Tests.Helpers
{
    /// <summary>
    /// Test Helper class with common test utilities
    /// </summary>
    internal static class RoutePlannerExtensions
    {
        /// <summary>
        /// Creates test case data from instance of <see cref="Route"/>
        /// </summary>
        /// <param name="route">Route to create <see cref="TestCaseData"/> from</param>
        /// <returns>Instance of <see cref="TestCaseData"/> with shuffled <paramref name="route"/> as argument, and <paramref name="route"/> as expected return value</returns>
        public static TestCaseData ToTestCaseData(this Route route)
        {
            return new TestCaseData(route.Shuffle())
                .Returns(route)
                .SetName($"Route with {route.Count} edges");
        }
    }
}