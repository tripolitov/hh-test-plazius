﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Plazius.RoutePlanner.Tests.Helpers
{
    /// <summary>
    /// Test helper extensions for enumerable
    /// </summary>
    internal static class EnumerableExtensions
    {
        /// <summary>
        /// Shuffles the <see cref="IEnumerable{T}"/> 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"><see cref="IEnumerable{T}"/> to shuffle</param>
        /// <param name="random">Random generator</param>
        /// <returns>Shuffled <see cref="IEnumerable{T}"/></returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random random = null)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if(random == null) random = new Random();

            var data = source.ToArray();
            if (data.Length > 1)
            {
                for (var i = data.Length - 1; i > 0; i--)
                {
                    var randomIndex = random.Next(i + 1);
                    var temp = data[i];
                    yield return data[randomIndex];
                    data[randomIndex] = temp;
                }
            }
            yield return data[0];
        }

        /// <summary>
        /// Shuffle tail of the <see cref="Route"/>
        /// </summary>
        /// <param name="route"><see cref="Route"/> which tail to shuffle</param>
        /// <param name="count">The number of Edges to keep in the beginning. 1 by default</param>
        /// <returns>Shuffled route</returns>
        public static Route Shuffle(this Route route, int count = 1)
        {
            if (route.IsEmpty) return route;

            var result = new Route();
            result.AddRange(route.Take(count));
            if (route.Count > count)
            {
                result.AddRange(route.Skip(count).Shuffle());
            }
            return result;

        }
    }
}
