﻿using System;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace Plazius.RoutePlanner.Tests
{
    [TestFixture]
    class EdgeTests
    {
        [Test]
        public void FactoryMethodFactoryMethodCreatesWaypointWithProvidedWaypoints()
        {
            Waypoint from = Guid.NewGuid().ToString();
            Waypoint to = Guid.NewGuid().ToString();

            var edge = Edge.Create(from, to);

            Assert.AreEqual(from, edge.From);
            Assert.AreEqual(to, edge.To);
        }

        [Test]
        public void DefaultPropertyReturnsEdgeWithDefaultWaypoints()
        {
            var edge = Edge.Default;

            Assert.AreEqual(Waypoint.Default, edge.From);
            Assert.AreEqual(Waypoint.Default, edge.To);
        }

        [Test]
        public void DefaultEdgeHasDefaultWaypoints()
        {
            var edge = default(Edge);

            Assert.AreEqual(Waypoint.Default, edge.From);
            Assert.AreEqual(Waypoint.Default, edge.To);
        }

        [Test]
        public void EdgeCtorThrowsArgumentExceptionForDefaultFromWaypoint()
        {
            Waypoint to = Guid.NewGuid().ToString();

            Assert.Throws<ArgumentException>(() => new Edge(Waypoint.Default, to));
        }

        [Test]
        public void EdgeCtorThrowsArgumentExceptionForDefaultToWaypoint()
        {
            Waypoint from = Guid.NewGuid().ToString();

            Assert.Throws<ArgumentException>(() => new Edge(from, Waypoint.Default));
        }

        [Test]
        public void EdgeCtorThrowsArgumentExceptionForSameWaypointUsedAsFromAndTo()
        {
            var waypoint = Guid.NewGuid().ToString();

            Waypoint from = waypoint;
            Waypoint to = waypoint;

            Assert.Throws<ArgumentException>(() => new Edge(from, to));
        }

        [Test]
        public void EdgeToStringReturnsWaypointNamesWithArrowBetween()
        {
            var fromName = Guid.NewGuid().ToString();
            var toName = Guid.NewGuid().ToString();

            var from = new Waypoint(fromName);
            var to = new Waypoint(toName);

            var edge = new Edge(from, to);

            Assert.AreEqual($"{fromName}->{toName}", edge.ToString());
        }


        [Test]
        public void EqualityOperatorReturnsTrueForDifferentEdgesWithSameWaypoints()
        {
            var from = new Waypoint(Guid.NewGuid().ToString());
            var to = new Waypoint(Guid.NewGuid().ToString());

            var left = new Edge(from, to);
            var right = new Edge(from, to);

            Assert.True(left == right);
        }

        [Test]
        public void EqualityOperatorReturnsFalseForDifferentEdgesWithDifferentWaypoints()
        {
            var left = new Edge(new Waypoint(Guid.NewGuid().ToString()), new Waypoint(Guid.NewGuid().ToString()));
            var right = new Edge(new Waypoint(Guid.NewGuid().ToString()), new Waypoint(Guid.NewGuid().ToString()));

            Assert.False(left == right);
        }

        [Test]
        public void NonEqualityOperatorReturnsFalseForDifferentEdgesWithSameWaypoints()
        {
            var from = new Waypoint(Guid.NewGuid().ToString());
            var to = new Waypoint(Guid.NewGuid().ToString());

            var left = new Edge(from, to);
            var right = new Edge(from, to);

            Assert.False(left != right);
        }

        [Test]
        public void NonEqualityOperatorReturnsTrueForDifferentEdgesWithDifferentWaypoints()
        {
            var left = new Edge(new Waypoint(Guid.NewGuid().ToString()), new Waypoint(Guid.NewGuid().ToString()));
            var right = new Edge(new Waypoint(Guid.NewGuid().ToString()), new Waypoint(Guid.NewGuid().ToString()));

            Assert.True(left != right);
        }
    }
}
