﻿using System;

namespace Plazius.RoutePlanner
{
    /// <summary>
    /// Connects two <see cref="Waypoint"/>
    /// </summary>
    public struct Edge
    {
        public static readonly Edge Default = default(Edge);

        /// <summary>
        /// Factory method to create new instance of <see cref="Edge"/>
        /// </summary>
        /// <param name="from">The <see cref="Waypoint"/> edges starts at</param>
        /// <param name="to">The <see cref="Waypoint"/> edge ends at</param>
        /// <returns>New instance of <see cref="Edge"/></returns>
        public static Edge Create(Waypoint from, Waypoint to)
        {
            return new Edge(from, to);
        }

        /// <summary>
        /// Creates new instance of <see cref="Edge"/> class with two waypoint
        /// </summary>
        /// <param name="from">The <see cref="Waypoint"/> edges starts at</param>
        /// <param name="to">The <see cref="Waypoint"/> edge ends at</param>
        public Edge(Waypoint from, Waypoint to)
        {
            if(from == Waypoint.Default) throw new ArgumentException("Can not use default Waypoint as from", nameof(from));
            if(to == Waypoint.Default) throw new ArgumentException("Can not use default Waypoint as to", nameof(to));
            if(from == to) throw new ArgumentException($"Can not have edge from {from} to {to}");

            From = @from;
            To = to;
        }

        /// <summary>
        /// Get <see cref="Waypoint"/> edge starts at
        /// </summary>
        public Waypoint From { get; }

        /// <summary>
        /// Get <see cref="Waypoint"/> edge ends at
        /// </summary>
        public Waypoint To { get; }

        #region Equality members

        /// <summary>
        /// Indicates whether instance of <see cref="Edge"/> and specified other <see cref="Edge"/> are equal
        /// </summary>
        /// <param name="other"><see cref="Edge"/> to compare with current instance</param>
        /// <returns>true if From and To <see cref="Waypoint"/> values of both Edges are equal; otherwize, false;</returns>
        public bool Equals(Edge other)
        {
            return From.Equals(other.From) && To.Equals(other.To);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Edge && Equals((Edge)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (From.GetHashCode() * 397) ^ To.GetHashCode();
            }
        }

        public static bool operator ==(Edge left, Edge right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Edge left, Edge right)
        {
            return !Equals(left, right);
        }

        #endregion

        /// <summary>
        /// Returns <see cref="T:System.String"/> representation of <see cref="Edge"/>
        /// </summary>
        /// <returns><see cref="T:System.String"/> instance which represents <see cref="Edge"/> using pattern {From}-&gt;{To}</returns>
        public override string ToString()
        {
            return $"{From}->{To}";
        }
    }
}