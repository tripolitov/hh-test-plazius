﻿using System;

namespace Plazius.RoutePlanner
{
    /// <summary>
    /// Represents error occured during <see cref="Route"/> optimization
    /// </summary>
    public class RoutePlannerException : Exception
    {
        private readonly Route m_Route;

        public RoutePlannerException(string message, Route route)
            : base(message)
        {
            m_Route = route;
        }

        /// <summary>
        /// Creates and returns a string representation of <see cref="RoutePlannerException"/> with route attached.
        /// </summary>
        public override string ToString()
        {
            return base.ToString() + Environment.NewLine + m_Route;
        }
    }

    /// <summary>
    /// The exception that is thrown when circle is detected while <see cref="Route"/> optimization
    /// </summary>
    public class CircleDetectedException : RoutePlannerException
    {
        /// <summary>
        /// Creates new instance of <see cref="CircleDetectedException"/> with the route where circle was detected
        /// </summary>
        /// <param name="route">Instance of <see cref="Route"/> where circle was detected</param>
        public CircleDetectedException(Route route) 
            : base("Circle was detected within route", route)
        {
        }
    }

    /// <summary>
    /// The exception that is thrown when circle is detected while <see cref="Route"/> optimization
    /// </summary>
    public class GapDetectedException : RoutePlannerException
    {
        /// <summary>
        /// Creates new instance of <see cref="GapDetectedException"/> with the route where gap was detected
        /// </summary>
        /// <param name="route">Instance of <see cref="Route"/> where gap was detected</param>
        public GapDetectedException(Route route) 
            : base("Gap was detected within route", route)
        {
        }
    }
}
