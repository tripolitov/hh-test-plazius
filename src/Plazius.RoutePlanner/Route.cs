using System.Collections.Generic;
using System.Text;

namespace Plazius.RoutePlanner
{
    public class Route : List<Edge>
    {
        public static readonly Route Empty = new Route();

        /// <summary>
        /// Creates new instance of empty <see cref="Route"/>
        /// </summary>
        public Route()
        {
            
        }

        /// <summary>
        /// Creates new instance of <see cref="Route"/> with provided edges
        /// </summary>
        /// <param name="collection"></param>
        public Route(IEnumerable<Edge> collection)
            : base (collection)
        {

        }

        /// <summary>
        /// Returns true if route has no edges; otherwise false;
        /// </summary>
        public bool IsEmpty => Count == 0;

        /// <summary>
        /// Returns <see cref="T:System.String"/> representation of <see cref="Route"/>
        /// </summary>
        /// <returns><see cref="T:System.String"/> instance which represents <see cref="Route"/>  
        /// for empty route empty string;
        /// for route with edges uses patterm {From}-&gt;{To}; {From}-&gt;{To};
        /// </returns>
        public override string ToString()
        {
            var sb = new StringBuilder();

            foreach (var edge in this)
            {
                if (sb.Length > 0) sb.Append("; ");
                sb.Append($"{edge}");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns new instance of <see cref="Route"/> by waypoints order
        /// </summary>
        /// <returns>Instance of <see cref="Route"/> optimized by waypoints order</returns>
        public Route Optimize()
        {
            var route = this;
            if (route.IsEmpty) return Route.Empty;

            var visited = new HashSet<Waypoint>();

            var result = new Route(route);
            for (var i = 0; i < result.Count - 1; i++)
            {
                visited.Add(result[i].From);

                bool found = false;
                for (var j = i + 1; j < result.Count; j++)
                {
                    if (result[i].To == result[j].From)
                    {
                        if (visited.Contains(result[j].To))
                        {
                            throw new CircleDetectedException(route);
                        }

                        swap(result, i + 1, j);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    throw new GapDetectedException(route);
                }
            }

            return result;
        }

        private static void swap<T>(IList<T> result, int left, int right)
        {
            var temp = result[left];
            result[left] = result[right];
            result[right] = temp;
        }
    }
}