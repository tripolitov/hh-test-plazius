﻿using System;

namespace Plazius.RoutePlanner
{
    /// <summary>
    /// Describes each point of route
    /// </summary>
    public struct Waypoint
    {
        public static readonly Waypoint Default = default(Waypoint);

        /// <summary>
        /// Factory method to create new instance of <see cref="Waypoint"/>
        /// </summary>
        /// <returns>New instance of <see cref="Waypoint"/></returns>
        public static Waypoint Create(string name)
        {
            return new Waypoint(name);
        }

        /// <summary>
        /// Creates new instance of <see cref="Waypoint"/> with specific name
        /// </summary>
        /// <param name="name">Waypoint name, must be not null and not empty</param>
        /// <exception cref="ArgumentNullException">If name is null</exception>
        /// <exception cref="ArgumentException">If name is empty or whitespace</exception>
        public Waypoint(string name)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Value cannot be null or whitespace.", nameof(name));
            Name = name;
        }

        /// <summary>
        /// implicit conversation from <see cref="Waypoint"/> to <see cref="String"/>
        /// </summary>
        /// <param name="waypoint"><see cref="Waypoint"/> which should be converted to <see cref="String"/></param>
        public static implicit operator string(Waypoint waypoint)
        {
            return waypoint.Name;
        }

        /// <summary>
        /// implicit conversation from <see cref="String"/> to <see cref="Waypoint"/>
        /// </summary>
        /// <param name="name"><see cref="String"/> which should be converted to <see cref="Waypoint"/></param>
        public static implicit operator Waypoint(string name)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            return new Waypoint(name);
        }
        
        /// <summary>
        /// Name of the <see cref="Waypoint"/>
        /// </summary>
        public string Name { get; }

        #region Equality members

        /// <summary>
        /// Indicates whether instance of <see cref="Waypoint"/> and specified other <see cref="Waypoint"/> are equal
        /// </summary>
        /// <param name="other"><see cref="Edge"/> to compare with current instance</param>
        /// <returns>true if Name  values of both <see cref="Waypoint"/> instances are equal; otherwize, false;</returns>
        public bool Equals(Waypoint other)
        {
            return string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Waypoint && Equals((Waypoint)obj);
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public static bool operator ==(Waypoint left, Waypoint right)
        {
            return Equals(left, right);
        } 

        public static bool operator !=(Waypoint left, Waypoint right)
        {
            return !Equals(left, right);
        }

        #endregion

        /// <summary>
        /// Returns <see cref="T:System.String"/> representation of <see cref="Waypoint"/>
        /// </summary>
        /// <returns><see cref="T:System.String"/> instance which represents <see cref="Waypoint"/> using Name</returns>
        public override string ToString()
        {
            return Name;
        }
    }
}